﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour {

	public TextAsset lyrics;
	private Dictionary< int, string > lyrics_timeline = new Dictionary< int, string> ();

	// Use this for initialization
	void Start () {

		//Parse the lyrics json
		lyrics_timeline = LyricsParser.ParseLyricsJSON (lyrics);
		DebugLyrics ();
	}

	// TEMPORARY
	void DebugLyrics(){
		foreach (KeyValuePair<int, string> line in lyrics_timeline) {
			Debug.Log (line.Key + " : " + line.Value);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
