﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Valve.VR;

[CustomEditor(typeof(ChoregraphyRecorder))]
public class ChoregraphyRecorderEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		ChoregraphyRecorder myScript = (ChoregraphyRecorder)target;
		if(GUILayout.Button("Record"))
		{
			myScript.StartRecording();
		}

		if(GUILayout.Button("Stop"))
		{
			myScript.StopRecording();
		}
	}
}


