﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;

public class LyricsParser {

	public static Dictionary< int, string > ParseLyricsJSON(TextAsset json_file){
		Dictionary< int, string > lyrics_timeline = null;

		if (json_file == null ) {
			Debug.LogError("<< LyricsParser >> ERROR : Parser could not load the file");
		}else{
			//Init the plugin with the path of the JSON
			SimpleJSON.JSONNode node = SimpleJSON.JSONNode.Parse (json_file.ToString());

			//Create a new dict
			lyrics_timeline = new Dictionary< int, string > ();

			using (IEnumerator<KeyValuePair< string, JSONNode >> enumerator = node.Linq.GetEnumerator()) {
				while (enumerator.MoveNext ()) {

					int seconds = ConvertToTimestamp (enumerator.Current.Value["timestamp"]);
					lyrics_timeline.Add(seconds,enumerator.Current.Value["lyric"]);
				}
			}

		}

		return lyrics_timeline;
	}

	// Take a string input formatted like "HH:mm:ss"
	// Output the total seconds 
	public static int ConvertToTimestamp(string input)
	{
		string date_format = "HH:mm:ss";
		DateTime parsed_date = DateTime.ParseExact (input, date_format, null);

		return parsed_date.Hour * 3600 + parsed_date.Minute * 60 + parsed_date.Second;
	}


}
