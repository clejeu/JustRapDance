﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ChoregraphyRecorder : MonoBehaviour
{
	public Valve.VR.InteractionSystem.Player player;
	public float seconds_between_record = 2.0f; // the number of seconds between to recorded positions

	private Valve.VR.InteractionSystem.Hand left_hand;
	private Valve.VR.InteractionSystem.Hand right_hand;
	private float timer = 0.0f;
	private bool _isRecording = false;
	private Dictionary<float, Transform> _left_positions;
	private Dictionary<float, Transform> _right_positions;
	private float _total_time = 0.0f;

	void Start(){
		_left_positions = new Dictionary<float, Transform> ();
		_right_positions = new Dictionary<float, Transform> ();

		// Register the hands 
		if (player.hands [0].startingHandType == Valve.VR.InteractionSystem.Hand.HandType.Left) {
			left_hand = player.hands [0];
			right_hand = player.hands [1];
		} else {
			left_hand = player.hands [1];
			right_hand = player.hands [0];
		}
	}

	void Update(){
		
	}

	public IEnumerator RecordAtInterval(){
		while (_isRecording) {
			_total_time += Time.deltaTime;

			//Store positions in dict
			_left_positions.Add(timer, left_hand.transform);
			_right_positions.Add(timer, right_hand.transform);

			yield return new WaitForSeconds( seconds_between_record );
		}
		yield return null;
	}

	public void StartRecording(){
		_isRecording = true;
		_total_time = 0.0f;
		Debug.Log ("Recording ...");
		StartCoroutine (RecordAtInterval());
	}

	public void StopRecording(){
		_isRecording = false;
		Debug.Log ("End of record ...");
		Debug.Log("Total number of positions recorded : " + _left_positions.Count);
		Debug.Log("Total duration : " + _total_time );
		LyricsWriter.Export (_left_positions, _right_positions);
	}
}


